import express from 'express';
import cors from 'cors';
import http from 'http';
import { Server as socketIO } from 'socket.io';
import { socketController } from '../sockets/controller.js';

export default class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.server = http.createServer(this.app);
    this.io = new socketIO(this.server);
    // MIDDLEWARES
    this.middlewares();
    // ROUTES
    this.routes();
    // SOCKETS
    this.sockets();
  }

  middlewares() {
    this.app.use(cors());
    this.app.use(express.static('public'));
  }

  routes() {
    //this.app.use(this.postPath, postsRouter);
  }

  sockets() {
    this.io.on('connection', socketController);
  }

  listen() {
    this.server.listen(this.port, () => {
      console.log(`Server is running on port ${this.port} :D!`);
    });
  }
}
